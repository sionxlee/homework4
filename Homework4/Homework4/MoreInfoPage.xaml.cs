﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework4.Model;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreInfoPage : ContentPage
	{
		public MoreInfoPage (Recipe recipe)
		{
			InitializeComponent ();
            BindingContext = recipe.Content;

        }
	}
}