﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Homework4.Model;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Recipes : ContentPage
	{
        ObservableCollection<Recipe> RecipeListView;


        public Recipes ()
        {
            InitializeComponent();

            RecipeList();
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Recipe itemTapped = (Recipe)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }

        void HandleDeleteClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var recipe = (Recipe)menuItem.CommandParameter;
            RecipeListView.Remove(recipe);

            RecipeCollection.ItemsSource = RecipeListView;
        }



        void HandleMoreClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var recipe = (Recipe)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreInfoPage(recipe));
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            RecipeList();
            RecipeCollection.IsRefreshing = false;
        }

        private void RecipeList()
        {
            RecipeListView = new ObservableCollection<Recipe>()
            {
                new Recipe()
                {
                    IconSource = ImageSource.FromFile("GarlicPrimeRib.png"),
                    RecipeName = "Garlic Prime Rib",
                    Description="Quick and easy marinade and so tasty, I was trusted with this recipe but I can't keep it to myself!",
                    url="http://allrecipes.com/recipe/56352/garlic-prime-rib/?internalSource=hub%20recipe&referringId=200&referringContentType=recipe%20hub",
                    context="Place the roast in a roasting pan with the fatty side up. In a small bowl, mix together the garlic, olive oil, salt, pepper and thyme. Spread the mixture over the fatty layer of the roast, and let the roast sit out until it is at room temperature, no longer than 1 hour.\nPreheat the oven to 500 degrees F (260 degrees C).\nBake the roast for 20 minutes in the preheated oven, then reduce the temperature to 325 degrees F(165 degrees C), and continue roasting for an additional 60 to 75 minutes.The internal temperature of the roast should be at 135 degrees F(57 degrees C) for medium rare.\nAllow the roast to rest for 10 or 15 minutes before carving so the meat can retain its juices.",
                },
               new Recipe()
                {
                    IconSource = ImageSource.FromFile("BakeZiti.png"),
                    RecipeName = "Baked Ziti",
                    Description="A lady I worked with brought this in one day, and it was a hit. Now it is the favorite of all my dinner guests. It's great for a covered dish dinner too. I have made this also without the meat, and it is well received.",
                    url="http://allrecipes.com/recipe/11758/baked-ziti-i/?clickId=right%20rail0&internalSource=rr_feed_recipe_sb&referringId=56352%20referringContentType%3Drecipe",
                    context="Place the roast in a roasting pan with the fatty side up. In a small bowl, mix together the garlic, olive oil, salt, pepper and thyme. Spread the mixture over the fatty layer of the roast, and let the roast sit out until it is at room temperature, no longer than 1 hour.\nPreheat the oven to 500 degrees F (260 degrees C).\nBake the roast for 20 minutes in the preheated oven, then reduce the temperature to 325 degrees F(165 degrees C), and continue roasting for an additional 60 to 75 minutes.The internal temperature of the roast should be at 135 degrees F(57 degrees C) for medium rare.\nAllow the roast to rest for 10 or 15 minutes before carving so the meat can retain its juices.",

               },
                new Recipe()
                {
                    IconSource = ImageSource.FromFile("StuffedPeppers.png"),
                    RecipeName = "Stuffed Peppers",
                    Description="Green peppers stuffed with ground beef and rice and topped with a seasoned tomato sauce",
                    url="http://allrecipes.com/recipe/16330/stuffed-peppers/?internalSource=hub%20recipe&referringId=200&referringContentType=recipe%20hub",
                    context="Place the roast in a roasting pan with the fatty side up. In a small bowl, mix together the garlic, olive oil, salt, pepper and thyme. Spread the mixture over the fatty layer of the roast, and let the roast sit out until it is at room temperature, no longer than 1 hour.\nPreheat the oven to 500 degrees F (260 degrees C).\nBake the roast for 20 minutes in the preheated oven, then reduce the temperature to 325 degrees F(165 degrees C), and continue roasting for an additional 60 to 75 minutes.The internal temperature of the roast should be at 135 degrees F(57 degrees C) for medium rare.\nAllow the roast to rest for 10 or 15 minutes before carving so the meat can retain its juices.",

                },
                new Recipe()
                {
                    IconSource = ImageSource.FromFile("BakeZiti.png"),
                    RecipeName = "Baked Ziti",
                    Description="A lady I worked with brought this in one day, and it was a hit. Now it is the favorite of all my dinner guests. It's great for a covered dish dinner too. I have made this also without the meat, and it is well received.",
                    url="http://allrecipes.com/recipe/11758/baked-ziti-i/?clickId=right%20rail0&internalSource=rr_feed_recipe_sb&referringId=56352%20referringContentType%3Drecipe",
                    context="Place the roast in a roasting pan with the fatty side up. In a small bowl, mix together the garlic, olive oil, salt, pepper and thyme. Spread the mixture over the fatty layer of the roast, and let the roast sit out until it is at room temperature, no longer than 1 hour.\nPreheat the oven to 500 degrees F (260 degrees C).\nBake the roast for 20 minutes in the preheated oven, then reduce the temperature to 325 degrees F(165 degrees C), and continue roasting for an additional 60 to 75 minutes.The internal temperature of the roast should be at 135 degrees F(57 degrees C) for medium rare.\nAllow the roast to rest for 10 or 15 minutes before carving so the meat can retain its juices.",

                },
                new Recipe()
                {
                    IconSource = ImageSource.FromFile("ChickenPotPie.png"),
                    RecipeName = "Chicken Pot Pie",
                    Description="A delicious chicken pie made from scratch with carrots, peas and celery.",
                    url="http://allrecipes.com/recipe/26317/chicken-pot-pie-ix/?internalSource=hub%20recipe&referringId=201&referringContentType=recipe%20hub",
                    context="Place the roast in a roasting pan with the fatty side up. In a small bowl, mix together the garlic, olive oil, salt, pepper and thyme. Spread the mixture over the fatty layer of the roast, and let the roast sit out until it is at room temperature, no longer than 1 hour.\nPreheat the oven to 500 degrees F (260 degrees C).\nBake the roast for 20 minutes in the preheated oven, then reduce the temperature to 325 degrees F(165 degrees C), and continue roasting for an additional 60 to 75 minutes.The internal temperature of the roast should be at 135 degrees F(57 degrees C) for medium rare.\nAllow the roast to rest for 10 or 15 minutes before carving so the meat can retain its juices.",

                },
                new Recipe()
                {
                    IconSource = ImageSource.FromFile("BakedTeriyakiChicken.png"),
                    RecipeName = "Baked Teriyaki Chicken",
                    Description="A much requested chicken recipe! Easy to double for a large group. Delicious!",
                    url="http://allrecipes.com/recipe/9023/baked-teriyaki-chicken/?internalSource=hub%20recipe&referringId=201&referringContentType=recipe%20hub",
                    context="Place the roast in a roasting pan with the fatty side up. In a small bowl, mix together the garlic, olive oil, salt, pepper and thyme. Spread the mixture over the fatty layer of the roast, and let the roast sit out until it is at room temperature, no longer than 1 hour.\nPreheat the oven to 500 degrees F (260 degrees C).\nBake the roast for 20 minutes in the preheated oven, then reduce the temperature to 325 degrees F(165 degrees C), and continue roasting for an additional 60 to 75 minutes.The internal temperature of the roast should be at 135 degrees F(57 degrees C) for medium rare.\nAllow the roast to rest for 10 or 15 minutes before carving so the meat can retain its juices.",

                },
                new Recipe()
                {
                    IconSource = ImageSource.FromFile("JuicyRoastedChicken.png"),
                    RecipeName = "Juicy Roasted Chicken",
                    Description="My grandmother's recipe for roasted chicken. We are German and she used to do it this way all the time. I never have had a chicken this juicy before; this little trick works and makes the people eating it go silent. It's funny. We nibble on the celery after.",
                    url="http://allrecipes.com/recipe/83557/juicy-roasted-chicken/?internalSource=recipe%20hub&referringId=201&referringContentType=recipe%20hub&clickId=cardslot%2023",
                    context="Place the roast in a roasting pan with the fatty side up. In a small bowl, mix together the garlic, olive oil, salt, pepper and thyme. Spread the mixture over the fatty layer of the roast, and let the roast sit out until it is at room temperature, no longer than 1 hour.\nPreheat the oven to 500 degrees F (260 degrees C).\nBake the roast for 20 minutes in the preheated oven, then reduce the temperature to 325 degrees F(165 degrees C), and continue roasting for an additional 60 to 75 minutes.The internal temperature of the roast should be at 135 degrees F(57 degrees C) for medium rare.\nAllow the roast to rest for 10 or 15 minutes before carving so the meat can retain its juices.",

                },
                new Recipe()
                {
                    IconSource = ImageSource.FromFile("ChefJohnsShakshuka.png"),
                    RecipeName = "Chef John's Shakshuka",
                    Description="This North African one-dish-meal is so fast, easy, and delicious. Be sure to cook your sauce until the veggies are nice and soft and sweet. Once the eggs go in, you can finish covered on the stove, or just pop the pan into a hot oven until they cook to your liking.",
                    url="http://allrecipes.com/recipe/245362/chef-johns-shakshuka/?internalSource=staff%20pick&referringId=87&referringContentType=recipe%20hub",
                    context="Place the roast in a roasting pan with the fatty side up. In a small bowl, mix together the garlic, olive oil, salt, pepper and thyme. Spread the mixture over the fatty layer of the roast, and let the roast sit out until it is at room temperature, no longer than 1 hour.\nPreheat the oven to 500 degrees F (260 degrees C).\nBake the roast for 20 minutes in the preheated oven, then reduce the temperature to 325 degrees F(165 degrees C), and continue roasting for an additional 60 to 75 minutes.The internal temperature of the roast should be at 135 degrees F(57 degrees C) for medium rare.\nAllow the roast to rest for 10 or 15 minutes before carving so the meat can retain its juices.",

                },
            };

            RecipeCollection.ItemsSource = RecipeListView;
        }
    }
}