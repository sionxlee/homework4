﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Homework4.Model
{
	public class Recipe : ContentPage
	{

            public string RecipeName
        {
            get;
            set;
        }

        public ImageSource IconSource
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string url
        {
            get;
            set;
        }

        public string context
        {
            get;
            set;
        }
    }
}