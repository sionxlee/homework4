﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework4
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        void Handle_NavigateToListView(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Recipes());
        }

    }
}
